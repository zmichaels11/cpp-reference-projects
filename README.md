# C++ Template Projects
List of Reference Projects to use as a template for creating new C++ Projects.
All projects linked here are licensed under the Unlicense and are public domain.

## How to use as a template
1) Clone the target repo
2) Run the unversion script
3) Rename the project and reinit version control

## Templates
| Project                                                                                       | Description                                               | Compatibility                                 |
| --------------------------------------------------------------------------------------------- | --------------------------------------------------------- | --------------------------------------------- |
| [Gradle C++](https://gitlab.com/zmichaels/gradle-cpp-project)                                 | Hello World built with Gradle                             | Linux                                         |
| [Gradle C++ Cross Compile](https://gitlab.com/zmichaels/Gradle_CPP_Cross_Compile_Demo)        | Cross Compiles for ARM Linux and Windows on x86 Linux     | Linux (arm, x86, amd64), Windows (x86, amd64) |
| [Gradle C++ GLFW OpenGL](https://gitlab.com/zmichaels/gradle_glfw_cpp)                        | Initializes OpenGL using GLFW and GLEW                    | Linux                                         |
| [Gradle C++ GLFW Volk Vulkan](https://gitlab.com/zmichaels/glfw_volk_vulkan_demo)             | Initializes Vulkan using GLFW and Volk                    | Linux, MacOS X                                |
| [Gradle C++ Volk Vulkan Compute](https://gitlab.com/zmichaels/volk_vulkan_compute_demo)       | Compute test implemented in Vulkan, initialized with Volk | Linux, MacOS X                                |
| [Gradle C++ JVM](https://gitlab.com/zmichaels/jvm_scripting_cpp)                              | Links JVM in C++ for scripting                            | Linux                                         |
| [Gradle C++ Volk VMA Vulkan Compute](https://gitlab.com/zmichaels/volk_vma_compute_demo)      | Vulkan demo using Volk and VMA                            | Linux, MacOS X                                |